package com.leaseplan.api.http;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

@DefaultUrl("service.url")
public class HttpGet {

    private final EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();

    String webserviceEndpoint = EnvironmentSpecificConfiguration.from(environmentVariables)
            .getProperty("service.url");

    @Step("Calling API endpoint")
    public void get(String arg0) {
        SerenityRest.given().get(webserviceEndpoint + arg0);
    }
}
