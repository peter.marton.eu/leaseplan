package com.leaseplan.api.stepdefinitions;

import com.leaseplan.api.http.HttpGet;
import com.leaseplan.api.model.DetailModel;
import com.leaseplan.api.model.ErrorResponseModel;
import com.leaseplan.api.model.FoodModel;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;


public class ProductSearchDefinition {

    @Steps
    public HttpGet httpGet;

    @When("the client calls the endpoint with parameter {string}")
    public void heCallsEndpoint(String product) {
        httpGet.get(product);
    }

    @Then("the client the results are displayed")
    public void theClientTheResultsDisplayedOfValidProduct() {
        Assert.assertNotNull(lastResponse().body().as(FoodModel[].class));
    }

    @Then("the client should receive HTTP {int} response code")
    public void clientSeesResponseCodeHttp200(int httpCode) {
        restAssuredThat(response -> response.statusCode(httpCode));
    }

    @And("the client the {string} is mentioned in the title")
    public void theClientTheResultsDisplayedForMango(String product) {
        restAssuredThat(response -> response.body("find {it.title}.title", containsStringIgnoringCase(product)));
    }

    @Then("the client got error response")
    public void theClientGotError() {
        Assert.assertNotNull(lastResponse().body().as(ErrorResponseModel.class));
    }

    @And("the client is not authorized")
    public void theClientIsNotAuthorized() {
        Assert.assertNotNull(lastResponse().body().as(DetailModel.class));
        restAssuredThat(response -> response.body("detail", equalTo("Not authenticated")));
    }
}
