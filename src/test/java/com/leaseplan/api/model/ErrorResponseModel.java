package com.leaseplan.api.model;


import com.fasterxml.jackson.annotation.JsonAlias;

import javax.inject.Named;

public class ErrorResponseModel {

    @JsonAlias("detail")
    public DetailModel detailModel;

    public static class DetailModel {

        public String error;
        public String message;
        @JsonAlias("requested_item")
        public String requestedItem;
        @JsonAlias("served_by")
        public String servedBy;
    }
}
