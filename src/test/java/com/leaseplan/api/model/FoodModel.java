package com.leaseplan.api.model;

import lombok.Data;

public class FoodModel {

    public String provider;
    public String title;
    public String url;
    public String brand;
    public double price;
    public String unit;
    public boolean isPromo;
    public String promoDetails;
    public String image;

}
