Feature: Search for the product

  @valid200
  Scenario Outline: the client searches for products
    When the client calls the endpoint with parameter "<product>"
    Then the client should receive HTTP 200 response code
    And the client the results are displayed
    And the client the "<product>" is mentioned in the title

    Examples:
      | product |
      | apple   |
      | mango   |
      | tofu    |
      | water   |


  @invalid404
  Scenario Outline: Using invalid products to test API endpoint
    When the client calls the endpoint with parameter "<product>"
    Then the client should receive HTTP 404 response code
    And the client got error response

    Examples:
      | product  |
      | car      |
      | airplane |

  @invalid401
  Scenario: Using invalid API endpoint
    When the client calls the endpoint with parameter ""
    Then the client should receive HTTP 401 response code
    And the client is not authorized

